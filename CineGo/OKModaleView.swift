//
//  OKModaleView.swift
//  CineGo
//
//  Created by Badass Pandas on 11/05/2022.
//

import SwiftUI

struct OKModaleView: View {
    
@Binding var sheetClosed:Bool

    
    var body: some View {
        
        ZStack {
            Color("darkAlba").ignoresSafeArea()
        
            VStack(spacing:40) {
                
                
                
                Spacer()
                
                
                
                HStack {
                    Image(systemName: "checkmark.circle.fill")
                        .foregroundColor(Color("roseAnne"))
                        .font(.system(size: 70))
                    Text("Votre réponse est enregistrée !")
                        .font(Font.custom("Comfortaa-Bold", size:24))
                        .lineSpacing(5)
                }
                .padding()
                
                
                
                VStack(alignment:.leading, spacing:15) {
                Text("Encore un peu de patience...")
                    .fontWeight(.bold)
                Text("Vous recevrez une notification dès que votre séance sera prête.")
//                    .fontWeight(.light)
                }
                .padding()
                
                Spacer()

                
                Button(action: {
                    sheetClosed.toggle()
                }, label: {
                    Text("OK")
                })
                    .font(Font.custom("Comfortaa-Bold", size:16))
                    .padding()
                    .frame(width:300)
                    .background(Color("bleuLoic"))
                    .cornerRadius(20)

                
            }
            .foregroundColor(Color("blancPanda"))
            
        }

    }
}

struct OKModaleView_Previews: PreviewProvider {
    static var previews: some View {
        OKModaleView(sheetClosed: .constant(false))
    }
}
