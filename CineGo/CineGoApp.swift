//
//  CineGoApp.swift
//  CineGo
//
//  Created by Badass Pandas on 10/05/2022.
//

import SwiftUI

@main
struct CineGoApp: App {
    var body: some Scene {
        WindowGroup {
            AccueilView()
        }
    }
}
