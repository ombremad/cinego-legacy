//
//  ListeView.swift
//  CineGo
//
//  Created by Badass Pandas on 11/05/2022.
//

import SwiftUI

struct ListeView: View {

    init(){
        UITableView.appearance().backgroundColor = .clear
    }

    
    var body: some View {

        ZStack {
            Color("darkAlba").ignoresSafeArea()
            
            VStack {
                
                Form() {
                    
                    
                    
                    Section(header:
                                Question(icone: "", question: "Mes claps en cours")
                    ) {
                            
                        NavigationLink(destination: ConfirmationView(clapPasse: false, dateClap: "nil")) {
                            HStack(spacing:15) {
                                Image(systemName: "play.fill")
                                    .foregroundColor(Color("roseAnne"))
                                    .frame(width: 30)
                                    Text("Clap de Loïc")
                            }
                        }
                            
                    }
                    .textCase(nil)
                    .foregroundColor(Color("blancPanda"))
                    .listRowBackground(Color("bleuLoic").opacity(0.3))
                    
                    
                    
                    Section(header:
                                Question(icone: "", question: "Mes claps terminés")
                    ) {
                            
                        NavigationLink(destination: ConfirmationView(clapPasse: true, dateClap: "Le 22 mars 2022 à 18h30")) {
                            
                        HStack(spacing:15) {
                            Image(systemName: "stop.fill")
                                .foregroundColor(Color("bleuLoic"))
                                .frame(width: 30)
                            VStack(alignment: .leading) {
                                Text("Clap de Paul")
                                Text("du 22 au 23 mars 2022")
                                    .font(.caption)
                            }
                        }
                        }
                        
                        NavigationLink(destination: ConfirmationView(clapPasse: true, dateClap: "Le 17 mars 2022 à 14h30")) {
                            
                        HStack(spacing:15) {
                            Image(systemName: "stop.fill")
                                .foregroundColor(Color("bleuLoic"))
                                .frame(width: 30)
                            VStack(alignment: .leading) {
                                Text("Clap de Pauline")
                                Text("du 17 mars 2022")
                                    .font(.caption)
                            }
                        }
                        }

                        NavigationLink(destination: ConfirmationView(clapPasse: true, dateClap: "Le 3 février 2022 à 22h55")) {
                            
                        HStack(spacing:15) {
                            Image(systemName: "stop.fill")
                                .foregroundColor(Color("bleuLoic"))
                                .frame(width: 30)
                            VStack(alignment: .leading) {
                                Text("Clap de Charlotte")
                                Text("du 1er au 3 février 2022")
                                    .font(.caption)
                            }
                        }
                        }

                            
                        
                        NavigationLink(destination: ConfirmationView(clapPasse: true, dateClap: "Le 17 janvier 2022 à 14h55")) {

                        HStack(spacing:15) {
                            Image(systemName: "stop.fill")
                                .foregroundColor(Color("bleuLoic"))
                                .frame(width: 30)
                            VStack(alignment: .leading) {
                                Text("Clap de Marie")
                                Text("du 16 au 18 janvier 2022")
                                    .font(.caption)
                            }
                        }
                        }
                       
                        
                        
                    }
                    .textCase(nil)
                    .foregroundColor(Color("blancPanda"))
                    .listRowBackground(Color("bleuLoic").opacity(0.3))

                    
                    
                    
                }
                
        
            }

            
            
            // Permet d'afficher le titre et la barre de navigation
            .navigationTitle("Liste")
            .toolbar {
                        ToolbarItem(placement: .principal) {
                            Text("Liste des Claps")
                                    .font(.headline)
                                    .foregroundColor(Color("blancPanda"))
                        }
                    }
            //

            
        }
            
    }
}


struct ListeView_Previews: PreviewProvider {
    static var previews: some View {
        ListeView()
    }
}
