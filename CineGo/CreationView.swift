//
//  CreationView.swift
//  CineGo
//
//  Created by Badass Pandas on 10/05/2022.
//

import SwiftUI

struct CreationView: View {
    
    // Données
    @Binding var backToRoot : Bool
    @State private var listeContactState = listeContact
    @State private var choixContacts = false
    @State private var choixCinema = 2
    
    // Gestion des DatePicker
    @State private var dateDebut : Date = Date.now // maintenant
    @State private var dateFin : Date = Date.now.addingTimeInterval(86_400) // dans 24 heures
    



    
    var body: some View {

    ZStack{
        Color("darkAlba").ignoresSafeArea()

        
    VStack {
    
        
        Form {
            
        Section(header:
        Question(icone: "iconePandaDate", question: "Quand ?"))
        {
                
            DatePicker(
                       "Du",
                       selection: $dateDebut,
                       in: Date.now...Date.now.addingTimeInterval(604_800)
                       )
                   .foregroundColor(Color("blancPanda"))
                   .environment(\.locale, Locale.init(identifier: "fr"))

            DatePicker(
                       "Au",
                       selection: $dateFin,
                       in: dateDebut.addingTimeInterval(9000)...dateDebut.addingTimeInterval(259_200)
                       )
                   .foregroundColor(Color("blancPanda"))
                   .environment(\.locale, Locale.init(identifier: "fr"))

        }
            
        

        .textCase(nil)
        .foregroundColor(Color("blancPanda"))
        .listRowBackground(Color("bleuLoic").opacity(0.3))

            
            
        Section(header:
        Question(icone: "iconePandaLieu", question: "Où ?"))
        {
                

            
            Picker("Votre cinéma :", selection: $choixCinema) {
                                Text("Le Méliès").tag(0)
                                Text("Le Vincennes Cinéma").tag(1)
                                Text("UGC Ciné Cité Bercy").tag(2)
                                Text("Cin'Hoche").tag(3)
                                Text("MK2 Bibliothèque").tag(4)
                                Text("Pathé Quai d'Ivry").tag(5)
            }

            
            
        }
        .textCase(nil)
        .foregroundColor(Color("blancPanda"))
        .listRowBackground(Color("bleuLoic").opacity(0.3))
        

        
            
        Section(header:
            Question(icone: "iconePandaAjout", question: "Avec qui ?")
        )
            {
                                
                List {
                    ForEach(Array(zip(listeContactState.indices, listeContactState)), id: \.0) { index, contact in
                        HStack {
                        Text(contact.nom)
                        Spacer()
                            Button(action: {
                                listeContactState[index].invitation.toggle()
                            }, label: {
                                Image(systemName: listeContactState[index].invitation ? "checkmark" : "" )
                            })
                        }
                        .foregroundColor(listeContactState[index].invitation ? Color("darkAlba") : Color("blancPanda") )
                        .listRowBackground(listeContactState[index].invitation ? Color("roseAnne") : Color("bleuLoic").opacity(0.3) )

                    }

                }

            }
            
            
            .textCase(nil)
            .foregroundColor(Color("blancPanda"))
            .listRowBackground(Color("bleuLoic").opacity(0.3))

            
            
            
        }
        
        
            
        
        
        
    Spacer()
        NavigationLink(destination: ParticipationView(backToRoot: $backToRoot)) {
        ModeleBouton(nomDuBouton: "Valider")
            }
        .isDetailLink(false)
            
            

        
            }


        
        }
        
        // Permet d'afficher le titre et la barre de navigation iOS
        .navigationTitle("Création")
        .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text("Création du Clap")
                                .font(.headline)
                                .foregroundColor(Color("blancPanda"))
                    }
                }
        //


        
    }

}



struct CreationView_Previews: PreviewProvider {
    static var previews: some View {
        CreationView(backToRoot: .constant(false))
    }
}
