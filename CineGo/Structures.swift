//
//  structures.swift
//  CineGo
//
//  Created by Badass Pandas on 11/05/2022.
//

import SwiftUI
import Foundation


// Pour le retour à la page parent

struct RootPresentationModeKey: EnvironmentKey {
    static let defaultValue: Binding<RootPresentationMode> = .constant(RootPresentationMode())
}

extension EnvironmentValues {
    var rootPresentationMode: Binding<RootPresentationMode> {
        get { return self[RootPresentationModeKey.self] }
        set { self[RootPresentationModeKey.self] = newValue }
    }
}

typealias RootPresentationMode = Bool

extension RootPresentationMode {
    
    public mutating func dismiss() {
        self.toggle()
    }
}

//





// Pour ouvrir les liens dans Maps

func openMap(adresse:String) {
    UIApplication.shared.open(URL(string: "https://maps.apple.com/?q=\(adresse.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")")!)
}

//





struct ModeleBouton: View {
    
    var nomDuBouton:String
    
    var body: some View {
        Text(nomDuBouton)
            .font(Font.custom("Comfortaa-Bold", size:16))
            .padding()
            .frame(width:300)
            .foregroundColor(Color("blancPanda"))
            .background(Color("bleuLoic"))
            .cornerRadius(20)
    }
}


struct Question: View {
    var icone: String
    var question : String
    
    var body: some View {
        HStack{
            Rectangle()
                .frame(width:0, height: 40)
                .foregroundColor(Color.clear)
            if icone.isEmpty == false {
            Image(icone)
                .resizable()
                .scaledToFit()
                .frame(height:25)
            }
            Text(question)
                .font(Font.custom("Comfortaa-Bold", size:20))
                .foregroundColor(Color("blancPanda"))
        }
    }
}
