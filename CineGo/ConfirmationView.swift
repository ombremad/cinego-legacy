//
//  ConfirmationView.swift
//  CineGo
//
//  Created by Badass Pandas on 10/05/2022.
//

import SwiftUI

struct ConfirmationView: View {
    
    let clapPasse : Bool
    let dateClap : String
    
    let randomFilm : Int = Int.random(in: 0...(filmMelies.count-1))
    let randomCine : Int = Int.random(in: 0...(cinemas.count-1))
    let randomSeance : Int = Int.random(in: 0...(filmMelies[0].seances.count-1))
    let randomParticipants : Int = Int.random(in: 2...11)
    
    
    var body: some View {
        ZStack(alignment:.top) {
            Color("darkAlba").ignoresSafeArea()
            Image(filmMelies[randomFilm].affiche)
                .resizable()
                .scaledToFit()
                .blur(radius: 25)
            
            Rectangle().ignoresSafeArea()
                .foregroundColor(Color("darkAlba").opacity(0.4))
            
            VStack {

                Spacer()
                
                HStack(alignment:.center, spacing: 30) {
                    Image(filmMelies[randomFilm].affiche)
                        .resizable()
                        .scaledToFit()
                        .frame(height:130)
                    
                        Text(filmMelies[randomFilm].titre)
                            .font(Font.custom("Comfortaa-Bold", size:35))
                        .lineSpacing(8)
                    
                    
                    
///                    Si on a le temps :
///                    Liste des icônes d'émotions
//                    VStack {
//                        if filmMelies[randomFilm].emotion
//                   }
                    
                    
                    
                }
                .font(.system(size: 35))
                
                Spacer()
                
                VStack {
                    
                    
                    
                HStack {
                    
                    
                    
                    Image(systemName: "mappin.and.ellipse")
                        .font(.system(size: 22))
                        .frame(width:35)
                    Text(cinemas[randomCine].nom)
                    
                    Spacer()
                    
                    Button(action: {
                        openMap(adresse: cinemas[randomCine].nom)
                        }, label: {
                        Text("Maps")
                            .fontWeight(.semibold)
                            .padding(10)
                            .frame(minWidth: 100)
                            .foregroundColor(Color("blancPanda"))
                            .background(Color("bleuLoic"))
                            .cornerRadius(20)
                        }
                    )
                }
                    
                    
                HStack {
                    
                    Image(systemName: "clock")
                        .font(.system(size: 22))
                        .frame(width:35)
                    if clapPasse == true {
                        Text(dateClap)
                    } else {
                        Text(filmMelies[randomFilm].seances[randomSeance])
                    }
                    
                    Spacer()
                    
                    
//                    Button(action: {
//
//                    }, label: {
//                        Text("Agenda")
//                            .fontWeight(.semibold)
//                    })
//                    .padding(10)
//                    .frame(minWidth: 100)
//                    .foregroundColor(Color("blancPanda"))
//                    .background(Color("bleuLoic"))
//                    .cornerRadius(20)
                }
            }
             
                Spacer()
                
                HStack(alignment:.top, spacing:25) {
                    VStack(spacing:5) {
                        Text("avec")
                        Text(String(randomParticipants))
                            .fontWeight(.heavy)
                            .font(.system(size: 40))
                            .padding()
                            .frame(height:50)
                            .foregroundColor(Color("darkTextes"))
                            .background(Color("roseAnne"))
                            .cornerRadius(20)
                    }
                    Image("iconePandaContent")
                        .resizable()
                        .scaledToFit()
                        .frame(height:90)
                }
                
                
                
                
        // Permet d'afficher le titre et la barre de navigation
        .navigationTitle("Séance")
        .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text("Ma séance")
                                .font(.headline)
                                .foregroundColor(Color("blancPanda"))
                    }
                }
        //

            

                
                Spacer()
                
                if clapPasse == true {
                } else {
                    Link(destination: URL(string: cinemas[randomCine].url)!) {
                        ModeleBouton(nomDuBouton: "Réserver")
                    }
                }

                
            }
            .foregroundColor(Color("blancPanda"))
            .padding()
        }
    }
}

struct ConfirmationView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmationView(clapPasse: false, dateClap: "Erreur")
    }
}
