//
//  Données.swift
//  CineGo
//
//  Created by Badass Pandas on 11/05/2022.
//

import SwiftUI


struct Clap : Identifiable {
    var id = UUID()
    let dateCreation : Date
    var dateDispo: [Date]
    var datePeremption : Date
    let listeParticipants : Int
}


var clap = Clap(dateCreation: Date(), dateDispo: [Date()], datePeremption: Date(), listeParticipants: 0)

//créer des instances de clap pour les afficher ensuite

struct Participant: Identifiable {
    var id = UUID()
    let nom : String
    let participation: ReponseParticipant
    var clapEnCours: [Clap]
    var clapPasses: [Clap]
    //listeView
    var invitation: Bool
    //toggle qui permet de selectionner la personne qu'on invite
}

struct ReponseParticipant {
    let confirmation : Bool = false
    var dateDebutDisponibilite : Date
    var dateFinDisponibilite : Date
    var preferences : [ListeEmotions]
    //participation de l'invité
}




var listeContact = [
    Participant(nom: "Abel Minouche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[0], emotions[2]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Anne-Laure Ferrari", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[0]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Bénédicte Maiche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[4], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Carole Bouquet", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[1], emotions[2], emotions[0]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Denise Caire", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Marie Rosello", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Bénédicte Maiche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Fabrice Champenault", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Gaëlle Perche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Henri Lecompte", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Ismael Leduc", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[4], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Jérôme Bicoque", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[2], emotions[0], emotions[4], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Lucille Duflot", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
    Participant(nom: "Mouloud Achour", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4], emotions[1]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
]



struct ListeEmotions: Identifiable {
    var id = UUID()
    var mood: String
    var score: Int = 0
    var selected: Bool
}


var emotions : [ListeEmotions] = [
    ListeEmotions(
        mood: "😀 Joyeux·se", //0
        selected: false
        ),
    ListeEmotions(
        mood: "👻 Hoouuuuuuu", //1
        selected: false
        ),
    ListeEmotions(
        mood: "😜 Plein·e d'énergie", //2
        selected: false
        ),
    ListeEmotions(
        mood: "🤣 PTDR", //3
        selected: false
        ),
    ListeEmotions(
        mood: "🎁 Surprenez-moi", //4
        selected: false
        ),
    ListeEmotions(
        mood: "🥰 Amoureux·se", //5
        selected: false
        )
    ]




struct ListeCinemas {
    //var idCine: UUID
    var nom: String
//    var adresse: String
    var url: String
    var films: [ListeFilms]
}


let cinemas : [ListeCinemas] = [
        ListeCinemas(
           // idCine:
            nom: "Le Méliès",
//            adresse: "12 place Jean Jaurès, 93100 Montreuil",
            url: "http://meliesmontreuil.fr/",
            films: filmMelies
        ),
        ListeCinemas(
            nom: "Le Vincennes Cinéma",
//            adresse: "30 avenue de Paris, 94300 Vincennes",
            url: "https://cinemalevincennes.com/a-l-affiche",
            films: filmVincennes
        ),
        ListeCinemas(
            nom: "UGC Ciné Cité Bercy",
//            adresse: "2 cour Saint-Émilion, 75012 Paris",
            url: "https://www.ugc.fr/cinema.html?id=12",
            films: filmUGCCineCiteBercy
        ),
        ListeCinemas(
            nom: "Cin'Hoche Bagnolet",
//            adresse: "6 rue Hoche, 93170 Bagnolet",
            url: "http://cinhoche.fr/FR/43/horaires-cinema-cinhoche-bagnolet.html",
            films: filmCinhoche
        ),
        ListeCinemas(
            nom: "mk2 Bibliothèque",
//            adresse: "128 avenue de France, 75013 Paris",
            url: "https://www.mk2.com/salles/916-mk2-bibliotheque",
            films: filmMK2Bibliotheque
        ),
        ListeCinemas(
            nom: "Pathé Quai d'Ivry",
//            adresse: "5 rue François Mitterrand, 75012 Paris",
            url: "https://www.cinemaspathegaumont.com/cinemas/cinema-pathe-quai-d-ivry",
            films: filmPatheQuaiDIvry
        )]

struct ListeFilms {
    //var idFilms = UUID()
    var titre: String
    var url: String
    var emotion: [ListeEmotions]
    var seances: [String]
    var affiche: String
    }

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

func formatDate(seance: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "fr_FR")
        dateFormatter.dateFormat = "EEEE dd MMM à HH:mm"
        let test = dateFormatter.string(from: seance)
        return test.capitalizingFirstLetter()
}



let filmMelies : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]

    
let filmVincennes : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]
                              
let filmUGCCineCiteBercy : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]

let filmCinhoche : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]

let filmMK2Bibliotheque : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]

let filmPatheQuaiDIvry : [ListeFilms] = [
            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
                       emotion: [emotions[0], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "downton abbey"
                      ),
            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "doctor-strange"
                      ),
            ListeFilms(titre: "Le Secret de la cité perdue",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
                       emotion: [emotions[2], emotions[5], emotions[3]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "le-secret-de-la-cite-perdue"),
            ListeFilms(titre: "Ténor",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
                       emotion: [emotions[0], emotions[4], emotions[5]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "Ténor"
                       ),
            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
                       emotion: [emotions[0], emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "bon-dieu"
                       ),
            ListeFilms(titre: "La Ruse",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "laRuse"
                       ),
            ListeFilms(titre: "Sonic 2 le film",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
                       emotion: [emotions[3], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sonic"
                      ),
            ListeFilms(titre: "The Northman",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
                       emotion: [emotions[2], emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "theNortman"
                      ),
            ListeFilms(titre: "Hit The Road",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "hitTheRoad"
                      ),
            ListeFilms(titre: "L'Affaire Collini",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "lAffaireCollini"
                      ),
            ListeFilms(titre: "Le Roi cerf",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
                       emotion: [emotions[0], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "roiCerf"
                      ),
            ListeFilms(titre: "Murina",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
                       emotion: [emotions[4], emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "murina"
                      ),
            ListeFilms(titre: "Contes du hasard et autres fantaisies",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
                       emotion: [emotions[5], emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "conteHasard"
                      ),
            ListeFilms(titre: "Sentinelle sud",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
                       emotion: [emotions[1]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "sentinelleSud"
                      ),
            ListeFilms(titre: "Vortex",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "vortex"
                      ),
            ListeFilms(titre: "Seule la terre est éternelle",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
                       emotion: [emotions[4]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "seuleLaTerre"
                      ),
            ListeFilms(titre: "Pulp Fiction",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
                       emotion: [emotions[2]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "pulpFiction"
                      ),
            ListeFilms(titre: "En même temps",
                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
                       emotion: [emotions[0]],
                       seances: [formatDate(seance: Date(timeIntervalSince1970: 1653035400)), formatDate(seance: Date(timeIntervalSince1970: 1653137100)), formatDate(seance: Date(timeIntervalSince1970: 1653228600)), formatDate(seance: Date(timeIntervalSince1970: 1653237600)), formatDate(seance: Date(timeIntervalSince1970: 1653330000)), formatDate(seance: Date(timeIntervalSince1970: 1653246600))],
                       affiche: "enMemeTemps"
                      )
                        ]
