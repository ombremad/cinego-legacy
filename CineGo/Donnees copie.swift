////
////  Données.swift
////  CineGo
////
////  Created by Apprenant 69 on 11/05/2022.
////
//
//import SwiftUI
////import Foundation
//
//
//struct Clap : Identifiable {
//    var id = UUID()
//    let dateCreation : Date
//    var dateDispo: [Date]
//    var datePeremption : Date
//    let listeParticipants : Int
//}
//
//
//var clap = Clap(dateCreation: Date(), dateDispo: [Date()], datePeremption: Date(), listeParticipants: 0)
//
////créer des instances de clap pour les afficher ensuite
//
//struct Participant: Identifiable {
//    var id = UUID()
//    let nom : String
//    let participation: ReponseParticipant
//    var clapEnCours: [Clap]
//    var clapPasses: [Clap]
//    //listeView
//    var invitation: Bool
//    //toggle qui permet de selectionner la personne qu'on invite
//}
//
//struct ReponseParticipant {
//    let confirmation : Bool = false
//    var dateDebutDisponibilite : Date
//    var dateFinDisponibilite : Date
//    var preferences : [ListeEmotions]
//    //participation de l'invité
//}
//
//
//
//
//var listeContact = [
//    Participant(nom: "Abel Minouche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[0], emotions[2]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Anne-Laure Ferrari", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[0]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Bénédicte Maiche", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[4], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Carole Bouquet", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[1], emotions[2], emotions[0]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Denise Caire", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Marie Rosello", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[3], emotions[0], emotions[4], emotions[5]]), clapEnCours: [clap], clapPasses: [clap], invitation: false),
//    Participant(nom: "Laura Wendels", participation: ReponseParticipant(dateDebutDisponibilite: Date(), dateFinDisponibilite: Date(), preferences: [emotions[1]]), clapEnCours: [clap], clapPasses: [clap], invitation: false)
//]
//
//
//
//struct ListeEmotions: Identifiable {
//    var id = UUID()
//    var mood: String
//    var score: Int = 0
//    var selected: Bool
//}
//
//
//var emotions : [ListeEmotions] = [
//    ListeEmotions(
//        mood: "Joie", //0
//        selected: false
//        ),
//    ListeEmotions(
//        mood: "Peur", //1
//        selected: false
//        ),
//    ListeEmotions(
//        mood: "Colère", //2
//        selected: false
//        ),
//    ListeEmotions(
//        mood: "Enthousiaste",
//        selected: false
//        ),
//    ListeEmotions(
//        mood: "Surprise",
//        selected: false
//        ),
//    ListeEmotions(
//        mood: "Amoureux·se",
//        selected: false
//        )
//    ]
//
//
//
//let contacts = [
//    "Abel Minouche",
//    "Anne-Laure Ferrari",
//    "Bénédicte Maiche",
//    "Carole Bouquet",
//    "Denise Caire",
//    "Emile Lucien",
//    "Fabrice Champenault",
//    "Gaëlle Perche",
//    "Henri Lecompte",
//    "Ismael Leduc",
//    "Jérôme Bicoque",
//    "Laura Wendels",
//    "Lucille Duflot",
//    "Marie Rosello",
//    "Mouloud Achour"
//]
//
//
//
//
//struct ListeCinemas {
//    //var idCine: UUID
//    var nom: String
//    var adresse: String
//    var url: String
//    var films: [ListeFilms]
//}
//
//
//let cinemas : [ListeCinemas] = [
//        ListeCinemas(
//           // idCine:
//            nom: "Le Méliès",
//            adresse: "12 place Jean Jaurès, 93100 Montreuil",
//            url: "http://meliesmontreuil.fr/",
//            films: filmMelies
//        ),
//        ListeCinemas(
//            nom: "Le Vincennes Cinéma",
//            adresse: "30 avenue de Paris, 94300 Vincennes",
//            url: "https://cinemalevincennes.com/a-l-affiche",
//            films: filmVincennes
//        ),
//        ListeCinemas(
//            nom: "UGC Ciné Cité Bercy",
//            adresse: "2 cour Saint-Émilion, 75012 Paris",
//            url: "https://www.ugc.fr/cinema.html?id=12",
//            films: filmUGC
//        ),
//        ListeCinemas(
//            nom: "Cin'Hoche Bagnolet",
//            adresse: "6 rue Hoche, 93170 Bagnolet",
//            url: "http://cinhoche.fr/FR/43/horaires-cinema-cinhoche-bagnolet.html",
//            films: filmCinhoche
//        )]
//
//struct ListeFilms {
//    //var idFilms = UUID()
//    var titre: String
//    var url: String
//    var emotion: [ListeEmotions]
//    var seances: [String]
//    var affiche: String
//        //seances.dateFormat = "yyyy/MM/dd HH:mm"
//    }
//
//func dateFormatFrancais (date: String) -> String {
//    
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
//    dateFormatter.locale = Locale(identifier: "fr_FR")
//    dateFormatter.timeZone = TimeZone(secondsFromGMT: 1)
//    
//    let dateFormat = dateFormatter.date(from: date)
//    
//    if let unwrapDate = dateFormat {
//        return unwrapDate.description
//    } else {
//        print("format not conform")
//        return Date().description
//    }
//    
//}
//
////let film1 = ListeFilms(titre: "", url: "", emotion: [emotions[2]], seances: [Date().formatted(date: .abbreviated, time: .shortened)])
//
//let filmMelies : [ListeFilms] = [
//            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
//                       emotion: [emotions[0], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "downton abbey"
//                      ),
//            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
//                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "doctor-strange"
//                      ),
//            ListeFilms(titre: "Le Secret de la cité perdue",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
//                       emotion: [emotions[2], emotions[5], emotions[3]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "le-secret-de-la-cite-perdue"),
//            ListeFilms(titre: "Ténor",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
//                       emotion: [emotions[0], emotions[4], emotions[5]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "Ténor"
//                       ),
//            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
//                       emotion: [emotions[0], emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "bon-dieu"
//                       ),
//            ListeFilms(titre: "La Ruse",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "laRuse"
//                       ),
//            ListeFilms(titre: "Sonic 2 le film",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
//                       emotion: [emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sonic"
//                      ),
//            ListeFilms(titre: "The Northman",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
//                       emotion: [emotions[2], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "theNortman"
//                      ),
//            ListeFilms(titre: "Hit The Road",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "hitTheRoad"
//                      ),
//            ListeFilms(titre: "L'Affaire Collini",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "lAffaireCollini"
//                      ),
//            ListeFilms(titre: "Le Roi cerf",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
//                       emotion: [emotions[0], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "roiCerf"
//                      ),
//            ListeFilms(titre: "Murina",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "murina"
//                      ),
//            ListeFilms(titre: "Contes du hasard et autres fantaisies",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
//                       emotion: [emotions[5], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "conteHasard"
//                      ),
//            ListeFilms(titre: "Sentinelle sud",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
//                       emotion: [emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sentinelleSud"
//                      ),
//            ListeFilms(titre: "Vortex",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "vortex"
//                      ),
//            ListeFilms(titre: "Seule la terre est éternelle",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "seuleLaTerre"
//                      ),
//            ListeFilms(titre: "Pulp Fiction",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "pulpFiction"
//                      ),
//            ListeFilms(titre: "En même temps",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
//                       emotion: [emotions[0]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "enMemeTemps"
//                      )
//                        ]
//
//    
//        let filmVincennes : [ListeFilms] = [
//            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
//                       emotion: [emotions[0], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "downton abbey"
//                      ),
//            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
//                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "doctor-strange"
//                      ),
//            ListeFilms(titre: "Le Secret de la cité perdue",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
//                       emotion: [emotions[2], emotions[5], emotions[3]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "le-secret-de-la-cite-perdue"),
//            ListeFilms(titre: "Ténor",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
//                       emotion: [emotions[0], emotions[4], emotions[5]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "Ténor"
//                       ),
//            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
//                       emotion: [emotions[0], emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "bon-dieu"
//                       ),
//            ListeFilms(titre: "La Ruse",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "laRuse"
//                       ),
//            ListeFilms(titre: "Sonic 2 le film",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
//                       emotion: [emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sonic"
//                      ),
//            ListeFilms(titre: "The Northman",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
//                       emotion: [emotions[2], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "theNortman"
//                      ),
//            ListeFilms(titre: "Hit The Road",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "hitTheRoad"
//                      ),
//            ListeFilms(titre: "L'Affaire Collini",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "lAffaireCollini"
//                      ),
//            ListeFilms(titre: "Le Roi cerf",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
//                       emotion: [emotions[0], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "roiCerf"
//                      ),
//            ListeFilms(titre: "Murina",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "murina"
//                      ),
//            ListeFilms(titre: "Contes du hasard et autres fantaisies",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
//                       emotion: [emotions[5], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "conteHasard"
//                      ),
//            ListeFilms(titre: "Sentinelle sud",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
//                       emotion: [emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sentinelleSud"
//                      ),
//            ListeFilms(titre: "Vortex",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "vortex"
//                      ),
//            ListeFilms(titre: "Seule la terre est éternelle",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "seuleLaTerre"
//                      ),
//            ListeFilms(titre: "Pulp Fiction",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "pulpFiction"
//                      ),
//            ListeFilms(titre: "En même temps",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
//                       emotion: [emotions[0]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "enMemeTemps"
//                      )
//                        ]
//                              
//        let filmUGC : [ListeFilms] = [
//            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
//                       emotion: [emotions[0], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "downton abbey"
//                      ),
//            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
//                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "doctor-strange"
//                      ),
//            ListeFilms(titre: "Le Secret de la cité perdue",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
//                       emotion: [emotions[2], emotions[5], emotions[3]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "le-secret-de-la-cite-perdue"),
//            ListeFilms(titre: "Ténor",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
//                       emotion: [emotions[0], emotions[4], emotions[5]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "Ténor"
//                       ),
//            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
//                       emotion: [emotions[0], emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "bon-dieu"
//                       ),
//            ListeFilms(titre: "La Ruse",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "laRuse"
//                       ),
//            ListeFilms(titre: "Sonic 2 le film",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
//                       emotion: [emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sonic"
//                      ),
//            ListeFilms(titre: "The Northman",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
//                       emotion: [emotions[2], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "theNortman"
//                      ),
//            ListeFilms(titre: "Hit The Road",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "hitTheRoad"
//                      ),
//            ListeFilms(titre: "L'Affaire Collini",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "lAffaireCollini"
//                      ),
//            ListeFilms(titre: "Le Roi cerf",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
//                       emotion: [emotions[0], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "roiCerf"
//                      ),
//            ListeFilms(titre: "Murina",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "murina"
//                      ),
//            ListeFilms(titre: "Contes du hasard et autres fantaisies",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
//                       emotion: [emotions[5], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "conteHasard"
//                      ),
//            ListeFilms(titre: "Sentinelle sud",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
//                       emotion: [emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sentinelleSud"
//                      ),
//            ListeFilms(titre: "Vortex",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "vortex"
//                      ),
//            ListeFilms(titre: "Seule la terre est éternelle",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "seuleLaTerre"
//                      ),
//            ListeFilms(titre: "Pulp Fiction",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "pulpFiction"
//                      ),
//            ListeFilms(titre: "En même temps",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
//                       emotion: [emotions[0]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "enMemeTemps"
//                      )
//                        ]
//
//        let filmCinhoche : [ListeFilms] = [
//            ListeFilms(titre: "Downton Abbey II : Une nouvelle ère",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=279664.html",
//                       emotion: [emotions[0], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "downton abbey"
//                      ),
//            ListeFilms(titre: "Doctor Strange in the Multiverse of Madness",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251390.html",
//                       emotion: [emotions[3], emotions[1], emotions[0], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "doctor-strange"
//                      ),
//            ListeFilms(titre: "Le Secret de la cité perdue",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=287082.html",
//                       emotion: [emotions[2], emotions[5], emotions[3]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "le-secret-de-la-cite-perdue"),
//            ListeFilms(titre: "Ténor",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293272.html",
//                       emotion: [emotions[0], emotions[4], emotions[5]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "Ténor"
//                       ),
//            ListeFilms(titre: "Qu'est-ce qu'on a tous fait au Bon Dieu ?",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=274484.html",
//                       emotion: [emotions[0], emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "bon-dieu"
//                       ),
//            ListeFilms(titre: "La Ruse",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278318.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "laRuse"
//                       ),
//            ListeFilms(titre: "Sonic 2 le film",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281203.html",
//                       emotion: [emotions[3], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sonic"
//                      ),
//            ListeFilms(titre: "The Northman",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=278182.html",
//                       emotion: [emotions[2], emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "theNortman"
//                      ),
//            ListeFilms(titre: "Hit The Road",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=293472.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "hitTheRoad"
//                      ),
//            ListeFilms(titre: "L'Affaire Collini",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=251646.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "lAffaireCollini"
//                      ),
//            ListeFilms(titre: "Le Roi cerf",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=292928.html",
//                       emotion: [emotions[0], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "roiCerf"
//                      ),
//            ListeFilms(titre: "Murina",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=264844.html",
//                       emotion: [emotions[4], emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "murina"
//                      ),
//            ListeFilms(titre: "Contes du hasard et autres fantaisies",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=290328.html",
//                       emotion: [emotions[5], emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "conteHasard"
//                      ),
//            ListeFilms(titre: "Sentinelle sud",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=281765.html",
//                       emotion: [emotions[1]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "sentinelleSud"
//                      ),
//            ListeFilms(titre: "Vortex",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=291311.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "vortex"
//                      ),
//            ListeFilms(titre: "Seule la terre est éternelle",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=299409.html",
//                       emotion: [emotions[4]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "seuleLaTerre"
//                      ),
//            ListeFilms(titre: "Pulp Fiction",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=10126.html",
//                       emotion: [emotions[2]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "pulpFiction"
//                      ),
//            ListeFilms(titre: "En même temps",
//                       url: "https://www.allocine.fr/film/fichefilm_gen_cfilm=297305.html",
//                       emotion: [emotions[0]],
//                       seances: [dateFormatFrancais (date: "20/05/2022 10:30"), dateFormatFrancais (date: "21/05/2022 14:45"),  dateFormatFrancais (date: "22/05/2022 16:10"), dateFormatFrancais(date: "22/05/2022 18:40"), dateFormatFrancais (date: "23/05/2022 20:20"), dateFormatFrancais(date: "22/05/2022 21:10")],
//                       affiche: "enMemeTemps"
//                      )
//                        ]
