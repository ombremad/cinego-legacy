//
//  ParticipationView.swift
//  CineGo
//
//  Created by Badass Pandas on 11/05/2022.
//

import SwiftUI

struct ParticipationView: View {
    
    // Données
    @State private var listeEmotionsState = emotions
    @Binding var backToRoot: Bool
    // Gestion des DatePicker
    @State private var dateDebut : Date = Date.now // maintenant
    @State private var dateFin : Date = Date.now.addingTimeInterval(86_400) // dans 24 heures

    
    
    
    // Pour le fonctionnement du menu "Je participe à la séance ?"
    @State private var participationSeance : Bool = true

    // Pour l'ouverture de la modale de confirmation
    @State private var activateSheet = false
    
    
    
    
    
    
    var body: some View {
        ZStack{
            Color("darkAlba").ignoresSafeArea()
            
            VStack {
                
                Form {
                    
                    
// * Cette section est commentée pour les besoins de la démo, la décommenter pour l'app "complète" !

//                    Section(header: Text("Je participe à la séance ?")
//                        .lineSpacing(5))
//                    {
//                        HStack{
//
//
//                            Button (action: {
//                                participationSeance = true
//                            }, label: {
//                                Text("Oui")
//                            })
//                                .font(Font.custom("Comfortaa-Bold", size:16))
//                                .padding()
//                                .frame(width:120)
//                                .foregroundColor(Color("blancPanda"))
//                                .background(participationSeance ? Color("bleuLoic") : Color("darkAlba"))
//                                .cornerRadius(20)
//                                .buttonStyle(BorderlessButtonStyle())
//
//
//
//                            Spacer()
//
//
//                            Button (action: {
//                                participationSeance = false
//                            }, label: {
//                                Text("Non")
//                            })
//                                .font(Font.custom("Comfortaa-Bold", size:16))
//                                .padding()
//                                .frame(width:120)
//                                .foregroundColor(participationSeance ? Color("blancPanda") : Color("darkAlba"))
//                                .background(participationSeance ? Color("darkAlba") : Color("roseAnne"))
//                                .cornerRadius(20)
//                                .buttonStyle(BorderlessButtonStyle())
//
//
//                        }
//
//                        .listRowBackground(Color(.clear))
//
//                    }
//                    .textCase(nil)
//                    .font(Font.custom("Comfortaa-Bold", size:25))


                    

                    // Condition qui permet de masquer les choix si la personne ne participe pas à la séance
                    if participationSeance == true {
                        

                        
// * Cette section est commentée pour les besoins de la démo, la décommenter pour l'app "complète" !

//                    Section(header:
//                                Question(icone: "iconePandaDate", question: "Mes disponibilités ?"))
//                    {
//
//                        DatePicker(
//                                   "Du",
//                                   selection: $dateDebut,
//                                   in: Date.now...Date.now.addingTimeInterval(604_800)
//                                   )
//                               .foregroundColor(Color("blancPanda"))
//                               .environment(\.locale, Locale.init(identifier: "fr"))
//
//
//                        DatePicker(
//                                   "Au",
//                                   selection: $dateFin,
//                                   in: dateDebut.addingTimeInterval(9000)...dateDebut.addingTimeInterval(259_200)
//                                   )
//                               .foregroundColor(Color("blancPanda"))
//                               .environment(\.locale, Locale.init(identifier: "fr"))
//
//
//                    }
//                    .listRowBackground(Color("bleuLoic").opacity(0.3))
//                    .textCase(nil)
                    
                    Section(header:
                    Question(icone: "iconePandaEmotion", question: "Je suis d'humeur"))
                        {
                                            
                            List {
                                ForEach(Array(zip(listeEmotionsState.indices, listeEmotionsState)), id: \.0) { index, emotion in
                                    HStack {
                                    Text(emotion.mood)
                                    Spacer()
                                        Button(action: {
                                            listeEmotionsState[index].selected.toggle()
                                        }, label: {
                                            Image(systemName: listeEmotionsState[index].selected ? "checkmark" : "" )
                                        })
                                    }
                                    .foregroundColor(listeEmotionsState[index].selected ? Color("darkAlba") : Color("blancPanda") )
                                    .listRowBackground(listeEmotionsState[index].selected ? Color("roseAnne") : Color("bleuLoic").opacity(0.3) )

                                }

                            }

                        }
                        
                        
                        .textCase(nil)
                        .foregroundColor(Color("blancPanda"))
                        .listRowBackground(Color("bleuLoic").opacity(0.3))
                    }
                
                
                // Terminer le if ici
            }
                
                
                

                
                Spacer()


                Button(
                    action: {
                        backToRoot.toggle()
                        activateSheet.toggle()
                    }, label: {
                        Text("Valider")
                    })
                    .font(Font.custom("Comfortaa-Bold", size:16))
                    .padding()
                    .frame(width:300)
                    .foregroundColor(Color("blancPanda"))
                    .background(Color("bleuLoic"))
                    .cornerRadius(20)

                
            }
            .foregroundColor(Color("blancPanda"))
           
            
            // Permet d'afficher le titre et la barre de navigation
            .navigationTitle("Participer")
            .toolbar {
                        ToolbarItem(placement: .principal) {
                            Text("Participation au Clap")
                                    .font(.headline)
                                    .foregroundColor(Color("blancPanda"))
                        }
                    }
            .sheet(isPresented: $activateSheet, content: {
                OKModaleView(sheetClosed: $activateSheet)
            })
            


            //

        }
    }
}

struct ParticipationView_Previews: PreviewProvider {
    static var previews: some View {
        ParticipationView(backToRoot: .constant(false))
    }
}

