//
//  AccueilView.swift
//  CineGo
//
//  Created by Badass Pandas on 10/05/2022.
//

import SwiftUI


struct AccueilView: View {
    
    
    @State private var root : Bool = false
    
    // Notification pour la démo
    @State private var demoNotification : Bool = false
    
    // Affichage de l'easter egg
    @State private var easterEgg : Int = 0
    
    

    
    
    var body: some View {
        
        
        
        NavigationView {
            
            ZStack(alignment: .top) {
                
                    Color("darkAlba").ignoresSafeArea()
                    Image("fond-films").ignoresSafeArea()
                
                VStack(spacing:20) {
                        HStack {
                                
                            NavigationLink (destination:ListeView()){
                                
                            // Bouton d'accès à la liste des Claps
                            Image (systemName: demoNotification ? "list.bullet.circle.fill" : "list.bullet.circle")
                                        .padding()
                                        .font(.system(size:50))
                                        .foregroundColor(demoNotification ? Color("roseAnne") : Color("blancPanda"))
                                    }
                            
                            Spacer()
                            
                            // Easter egg (taper 10 fois la tête du panda)
                                if easterEgg == 10 {
                                    let monospaceFont = Font
                                            .system(size: 11)
                                            .monospaced()
                                    Text("badassPandas = [\"Alba\",\"Anne\",\"Loïc\"]")
                                            .padding(.horizontal)
                                            .font(monospaceFont)
                                            .background(Color.black)
                                }

                            Spacer()
                            
                            // Bouton invisible pour déclencher la notification
                            Image (systemName: "circle.fill")
                                .padding()
                                .font(.system(size: 50))
                                .foregroundColor(.clear)
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    demoNotification.toggle()
                                }
                            
                        }
                          
                    Spacer()
                    Spacer()

                        
                        Image ("logoCineGo")
                                .resizable()
                                .scaledToFit()
                                .frame(height:130)
                                .onTapGesture {
                                    if easterEgg < 10 {
                                        easterEgg += 1
                                } else {
                                    easterEgg = 0
                                    }
                                }

                        
                        
                        Text ("CinéGo")
                            .font(Font.custom("FairyMuffinRoundPop", size:40))
                            .foregroundColor(Color("blancPanda"))
                        
                        Text ("Invite tes ami·e·s,\nréserve ta place de ciné !")
                            .font(Font.custom("Comfortaa", size:20))
                            .foregroundColor(Color("blancPanda"))
                            .multilineTextAlignment(.center)
                            .lineSpacing(5)

                                                
                    Spacer()
                        
                    NavigationLink (destination:CreationView(backToRoot: $root), isActive: $root){
                            ModeleBouton(nomDuBouton: "Créer un Clap")
                            }
                    .isDetailLink(false)

                        
                        }
                    .padding()
                
                }
            .navigationTitle("")
            .navigationBarHidden(true)
            .navigationBarTitleDisplayMode(.inline)

        }
    }
}

struct AccueilView_Previews: PreviewProvider {
    static var previews: some View {
        AccueilView()
            .previewDevice("iPhone 12")
    }
}

